@extends('layouts.main')

@section('title', 'Rak')

@section('content')
<h1>Rak</h1>
@if (auth()->user()->role == 'petugas')
    <a href="{{route('rak.create')}}" class="btn btn-primary" role="button">Tambah rak</a>
@endif
<table class="table table-striped border mt-3">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Rak</th>
            <th scope="col">Lokasi Rak</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $key => $rak)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$rak->nama_rak}}</td>
            <td>{{$rak->lokasi_rak}}</td>
            <td>
                <form action="{{route('rak.destroy', $rak->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <a href="{{route('rak.show', $rak->id)}}" class="btn btn-info" role="button">Lihat</a>
                    @if (auth()->user()->role == 'petugas')
                        <a href="{{route('rak.edit', $rak->id)}}" class="btn btn-warning" role="button">Edit</a>
                        <a href="{{route('rak.destroy', $rak->id)}}" onclick="event.preventDefault(); this.closest('form').submit();" class="btn btn-danger" role="button">Hapus</a>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
