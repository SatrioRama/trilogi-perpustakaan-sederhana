@extends('layouts.main')

@section('title', 'Pinjaman')

@section('content')
<h1>Pinjaman</h1>
<h3>{{$pinjaman->relasi_anggota->name}}</h3><a href="{{route('pinjaman.index')}}">Kembali</a>
<table class="table table-striped mt-3">
    <tbody>
        <tr>
            <td>Nama Peminjam</td>
            <td>{{$pinjaman->relasi_anggota->name}}</td>
        </tr>
        <tr>
            <td>Judul Buku</td>
            <td>{{$pinjaman->relasi_buku->judul_buku}}</td>
        </tr>
        <tr>
            <td>Jumlah Dipinjam</td>
            <td>{{$pinjaman->jumlah}}</td>
        </tr>
        <tr>
            <td>Nama Petugas</td>
            <td>{{$pinjaman->relasi_petugas->name}}</td>
        </tr>
        <tr>
            <td>Tanggal Pinjam</td>
            <td>{{date('d-M-Y', strtotime($pinjaman->tanggal_pinjam))}}</td>
        </tr>
        <tr>
            <td>Tanggal Rencana Kembali</td>
            <td>{{date('d-M-Y', strtotime($pinjaman->tanggal_rencana_kembali))}}</td>
        </tr>
        <tr>
            <td>Aksi</td>
            <td>
                <form action="{{route('pinjaman.destroy', $pinjaman->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    @if (auth()->user()->role == 'petugas')
                        <a href="{{route('pinjaman.edit', $pinjaman->id)}}" class="btn btn-warning" role="button">Edit</a>
                        <a href="{{route('pinjaman.destroy', $pinjaman->id)}}" onclick="event.preventDefault(); this.closest('form').submit();" class="btn btn-danger" role="button">Hapus</a>
                    @endif
                </form>
            </td>
        </tr>
    </tbody>
</table>
@endsection
