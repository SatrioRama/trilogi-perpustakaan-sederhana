@extends('layouts.main')

@section('title', 'Pengembalian')

@section('content')
<h1>Pengembalian</h1>
<table class="table table-striped border mt-3">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Peminjam</th>
            <th scope="col">Judul Buku</th>
            <th scope="col">Jumlah dipinjam</th>
            <th scope="col">Tanggal Pinjam</th>
            <th scope="col">Tanggal Rencana Kembali</th>
            <th scope="col">Tanggal pengembalian</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $key => $kembali)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$kembali->relasi_anggota->name}}</td>
            <td>{{$kembali->relasi_buku->judul_buku}}</td>
            <td>{{$kembali->jumlah}}</td>
            <td>{{date('d-M-Y', strtotime($kembali->tanggal_pinjam))}}</td>
            <td>{{date('d-M-Y', strtotime($kembali->tanggal_rencana_kembali))}}</td>
            @if (!empty($kembali->tanggal_kembali))
                <td>{{date('d-M-Y', strtotime($kembali->tanggal_kembali))}}</td>
            @else
                <td>Belum dikembalikan</td>
            @endif
            <td>
                <form action="{{route('pengembalian.update', $kembali->id)}}" method="post">
                    @csrf
                    @method('PATCH')
                    <a href="{{route('pengembalian.update', $kembali->id)}}" onclick="event.preventDefault(); this.closest('form').submit();" class="btn btn-success" role="button">Kembalikan</a>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
