<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $petugas = User::create([
            'name'      => 'Petugas 1',
            'email'     => 'petugas@trilogi.ac.id',
            'password'  => bcrypt('password'),
            'role'      => 'petugas',
        ]);

        $anggota = User::create([
            'name'      => 'Anggota 1',
            'email'     => 'anggota@trilogi.ac.id',
            'password'  => bcrypt('password'),
            'role'      => 'anggota',
        ]);
    }
}
