@extends('layouts.main')

@section('title', 'Pinjaman')

@section('content')
<h1>Tambah Pinjaman</h1>
<form action="{{ route('pinjaman.store') }}" method="POST">
    @csrf
    <div class="form-group mt-3">
        <label>Peminjam</label>
        <select class="form-control" name="anggota_id">
            @foreach (\App\Models\User::where('role', '=', 'anggota')->get() as $anggota)
                <option value="{{$anggota->id}}">{{$anggota->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group mt-3">
        <label>Judul Buku</label>
        <select class="form-control" name="buku_id">
            @foreach (\App\Models\Buku::where('stok', '>', 0)->get() as $buku)
                <option value="{{$buku->id}}">{{$buku->kode_buku}} - {{$buku->judul_buku}}</option>
            @endforeach
        </select>
    </div>
    <input type="hidden" value="{{auth()->user()->id}}" class="form-control" name="petugas_id" required>
    <div class="form-group mt-3">
        <label>Jumlah dipinjam</label>
        <input type="number" class="form-control" name="jumlah" required>
    </div>
    <div class="form-group mt-3">
        <label>Tanggal Pinjam</label>
        <input type="date" class="form-control" name="tanggal_pinjam" required>
    </div>
    <div class="form-group mt-3">
        <label>Tanggal Rencana Dikembalikan</label>
        <input type="date" class="form-control" name="tanggal_rencana_kembali" required>
    </div>
    <button type="submit" class="btn btn-primary mt-3">Submit</button>
</form>

@endsection
