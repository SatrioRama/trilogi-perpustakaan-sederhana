@extends('layouts.main')

@section('title', 'Dashboard')

@section('content')
    <div class="text-center">
        <h1>Selamat datang di dashboard</h1>
        <h3>{{auth()->user()->role}}</h3>
        <h3>{{auth()->user()->name}}</h3>
    </div>
    @include('buku-popular')
    @if (auth()->user()->role == 'anggota')
        @include('buku-terpinjam')
    @endif
@endsection
