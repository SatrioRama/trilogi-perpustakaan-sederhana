@extends('layouts.main')

@section('title', 'Rak')

@section('content')
<h1>Tambah Rak</h1>
<form action="{{ route('rak.store') }}" method="POST">
    @csrf
    <div class="form-group mt-3">
        <label for="exampleInputName1">Nama Rak</label>
        <input type="text" class="form-control" name="nama_rak" required>
    </div>
    <div class="form-group mt-3">
        <label for="exampleInputName1">Lokasi Rak</label>
        <input type="text" class="form-control" name="lokasi_rak" required>
    </div>
    <button type="submit" class="btn btn-primary mt-3">Submit</button>
</form>

@endsection
