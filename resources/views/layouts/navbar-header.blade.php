<nav class="navbar navbar-expand-lg bg-light">
    <div class="container-fluid container">
        <a class="navbar-brand" href="{{url('/dashboard')}}">Perpustakaan Trilogi</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('dashboard') ? 'active' : '' }}" aria-current="page" href="{{url('/dashboard')}}">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('user.index') ? 'active' : '' }}" aria-current="page" href="{{url('/user')}}">User</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('rak.index') ? 'active' : '' }}" aria-current="page" href="{{url('/rak')}}">Rak</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('buku.index') ? 'active' : '' }}" aria-current="page" href="{{url('/buku')}}">Buku</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('pinjaman.index') ? 'active' : '' }}" aria-current="page" href="{{url('/pinjaman')}}">Pinjaman</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('pengembalian.index') ? 'active' : '' }}" aria-current="page" href="{{url('/pengembalian')}}">Pengembalian</a>
                </li>
            </ul>
        </div>
        <form method="POST" class="form-inline my-2 my-lg-0" action="{{ route('logout') }}">
            @csrf
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
                <div class="d-flex align-items-center">
                    <div class="ms-3"><span>Logout</span></div>
                </div>
            </a>
        </form>
    </div>
</nav>
