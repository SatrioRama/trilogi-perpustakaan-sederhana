@extends('layouts.main')

@section('title', 'Buku')

@section('content')
<h1>Buku</h1>
<h3>{{$buku->kode_buku}}</h3><a href="{{route('buku.index')}}">Kembali</a>
<table class="table table-striped mt-3">
    <tbody>
        <tr>
            <td>Kode Buku</td>
            <td>{{$buku->kode_buku}}</td>
        </tr>
        <tr>
            <td>Judul Buku</td>
            <td>{{$buku->judul_buku}}</td>
        </tr>
        <tr>
            <td>Penulis Buku</td>
            <td>{{$buku->penulis_buku}}</td>
        </tr>
        <tr>
            <td>Penerbit Buku</td>
            <td>{{$buku->penerbit_buku}}</td>
        </tr>
        <tr>
            <td>Tahun Terbit</td>
            <td>{{$buku->tahun_penerbit}}</td>
        </tr>
        <tr>
            <td>Stok Buku</td>
            <td>{{$buku->stok}}</td>
        </tr>
        <tr>
            <td>Lokasi Buku</td>
            <td>{{$buku->relasi_rak->nama_rak}} - {{$buku->relasi_rak->lokasi_rak}}</td>
        </tr>
        <tr>
            <td>Aksi</td>
            <td>
                <form action="{{route('buku.destroy', $buku->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    @if (auth()->user()->role == 'petugas')
                        <a href="{{route('buku.edit', $buku->id)}}" class="btn btn-warning" role="button">Edit</a>
                        <a href="{{route('buku.destroy', $buku->id)}}" onclick="event.preventDefault(); this.closest('form').submit();" class="btn btn-danger" role="button">Hapus</a>
                    @endif
                </form>
            </td>
        </tr>
    </tbody>
</table>
@endsection
