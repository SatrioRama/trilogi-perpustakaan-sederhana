@extends('layouts.main')

@section('title', 'User')

@section('content')
<h1>Edit User</h1>
<form action="{{ route('user.update', $data->id) }}" method="POST">
    @csrf
    @method('PATCH')
    <div class="form-group mt-3">
        <label for="exampleInputName1">Nama</label>
        <input type="text" class="form-control" name="name" required value="{{$data->name}}">
    </div>
    <div class="form-group mt-3">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" name="email" required value="{{$data->email}}">
    </div>
    <div class="form-group mt-3">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" name="password" required>
    </div>
    <div class="form-group mt-3">
        <label for="exampleFormControlSelect1">Role</label>
        <select class="form-control" name="role">
          <option value="anggota" @if ($data->role == 'anggota') selected @endif>Anggota</option>
          <option value="petugas" @if ($data->role == 'petugas') selected @endif>Petugas</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary mt-3">Submit</button>
</form>

@endsection
