<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Pinjaman;
use Illuminate\Http\Request;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->role == 'anggota') {
            $data = Pinjaman::where('tanggal_kembali', '=', null)->where('anggota_id', '=', auth()->user()->id)->get();
        } else {
            $data = Pinjaman::where('tanggal_kembali', '=', null)->get();
        }
        return view('pinjaman.list', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pinjaman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //untuk CRUD
        $create = Pinjaman::create($request->all());

        //untuk stok
        $update = Buku::find($request->buku_id);
        $update->stok -= $request->jumlah;
        $update->update();

        return redirect()->route('pinjaman.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function show(Pinjaman $pinjaman)
    {
        return view('pinjaman.show', compact('pinjaman'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function edit(Pinjaman $pinjaman)
    {
        return view('pinjaman.edit', compact('pinjaman'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pinjaman $pinjaman)
    {
        //mengembalikan stok
        $restok = Buku::find($pinjaman->buku_id);
        $restok->stok += $pinjaman->jumlah;
        $restok->update();

        $pinjaman->update($request->all());

        //untuk stok
        $update = Buku::find($request->buku_id);
        $update->stok -= $request->jumlah;
        $update->update();

        return redirect()->route('pinjaman.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pinjaman $pinjaman)
    {
        //mengembalikan stok
        $restok = Buku::find($pinjaman->buku_id);
        $restok->stok += $pinjaman->jumlah;
        $restok->update();

        $pinjaman->delete();

        return redirect()->route('pinjaman.index');
    }
}
