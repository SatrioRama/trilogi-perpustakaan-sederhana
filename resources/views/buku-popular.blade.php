<table class="table table-striped border mt-3">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Judul Buku</th>
            <th scope="col">Jumlah total terpinjam</th>
            <th scope="col">Jumlah total dikembalikan</th>
            <th scope="col">Stok Buku</th>
            <th scope="col">Lokasi Buku</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($buku_popular as $key => $buku)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$buku->judul_buku}}</td>
            <td>{{$buku->transaksi->sum('jumlah')}}</td>
            <td>{{$buku->transaksi->where('tanggal_kembali', '!=', null)->sum('jumlah')}}</td>
            <td>{{$buku->stok}}</td>
            <td>{{$buku->relasi_rak->nama_rak}} - {{$buku->relasi_rak->lokasi_rak}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
