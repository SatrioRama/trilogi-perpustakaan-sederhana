<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\PengembalianController;
use App\Http\Controllers\PinjamanController;
use App\Http\Controllers\RakController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function(){
    Route::redirect('/', '/login');
    Route::redirect('/home', '/login');
    Route::has('register');
});

Route::group(['middleware' => ['auth:sanctum', 'verified']], function(){
    Route::redirect('/', '/dashboard');
    Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
    Route::resources([
        'user' => UserController::class,
        'rak' => RakController::class,
        'buku' => BukuController::class,
        'pinjaman' => PinjamanController::class,
        'pengembalian' => PengembalianController::class,
    ]);
});

