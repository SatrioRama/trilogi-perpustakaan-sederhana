@extends('layouts.main')

@section('title', 'Rak')

@section('content')
<h1>Rak</h1>
<h3>{{$rak->nama_rak}}</h3><a href="{{route('rak.index')}}">Kembali</a>
<table class="table table-striped mt-3">
    <tbody>
        <tr>
            <td>Nama Rak</td>
            <td>{{$rak->nama_rak}}</td>
        </tr>
        <tr>
            <td>Lokasi Rak</td>
            <td>{{$rak->lokasi_rak}}</td>
        </tr>
        <tr>
            <td>Aksi</td>
            <td>
                <form action="{{route('rak.destroy', $rak->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    @if (auth()->user()->role == 'petugas')
                        <a href="{{route('rak.edit', $rak->id)}}" class="btn btn-warning" role="button">Edit</a>
                        <a href="{{route('rak.destroy', $rak->id)}}" onclick="event.preventDefault(); this.closest('form').submit();" class="btn btn-danger" role="button">Hapus</a>
                    @endif
                </form>
            </td>
        </tr>
    </tbody>
</table>
@endsection
