<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->id();
            $table->char('kode_buku', 5);
            $table->string('judul_buku', 50);
            $table->string('penulis_buku', 50);
            $table->string('penerbit_buku', 50);
            $table->integer('tahun_penerbit')->nullable();
            $table->integer('stok')->nullable();
            $table->integer('rak_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
