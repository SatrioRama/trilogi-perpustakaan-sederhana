<table class="table table-striped border mt-3">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Judul Buku</th>
            <th scope="col">Jumlah Pinjam</th>
            <th scope="col">Tanggal Pinjam</th>
            <th scope="col">Harus dekembalikan sebelum</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($masih_terpinjam as $key => $pinjam)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$pinjam->relasi_buku->judul_buku}}</td>
            <td>{{$pinjam->jumlah}}</td>
            <td>{{date('d-M-Y', strtotime($pinjam->tanggal_pinjam))}}</td>
            <td>{{date('d-M-Y', strtotime($pinjam->tanggal_rencana_kembali))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
