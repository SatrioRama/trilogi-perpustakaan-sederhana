@extends('layouts.main')

@section('title', 'User')

@section('content')
<h1>User</h1>
<h3>{{$data->name}}</h3><a href="{{route('user.index')}}">Kembali</a>
<table class="table table-striped mt-3">
    <tbody>
        <tr>
            <td>Nama</td>
            <td>{{$data->name}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$data->email}}</td>
        </tr>
        <tr>
            <td>Role</td>
            <td>{{$data->role}}</td>
        </tr>
        <tr>
            <td>Aksi</td>
            <td>
                <form action="{{route('user.destroy', $data->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <a href="{{route('user.edit', $data->id)}}" class="btn btn-warning" role="button">Edit</a>
                    @if (auth()->user()->role == 'petugas')
                        <a href="{{route('user.destroy', $data->id)}}" onclick="event.preventDefault(); this.closest('form').submit();" class="btn btn-danger" role="button">Hapus</a>
                    @endif
                </form>
            </td>
        </tr>
    </tbody>
</table>
@endsection
