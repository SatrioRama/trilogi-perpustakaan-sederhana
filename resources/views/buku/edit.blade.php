@extends('layouts.main')

@section('title', 'Buku')

@section('content')
<h1>Edit Buku</h1>
<form action="{{ route('buku.update', $buku->id) }}" method="POST">
    @csrf
    @method('PATCH')
    <div class="form-group mt-3">
        <label>Kode Buku</label>
        <input type="text" class="form-control" name="kode_buku" value="{{$buku->kode_buku}}" maxlength="5" required>
    </div>
    <div class="form-group mt-3">
        <label>Judul Buku</label>
        <input type="text" class="form-control" name="judul_buku" value="{{$buku->judul_buku}}" required>
    </div>
    <div class="form-group mt-3">
        <label>Penulis Buku</label>
        <input type="text" class="form-control" name="penulis_buku" value="{{$buku->penulis_buku}}" required>
    </div>
    <div class="form-group mt-3">
        <label>Penerbit Buku</label>
        <input type="text" class="form-control" name="penerbit_buku" value="{{$buku->penerbit_buku}}" required>
    </div>
    <div class="form-group mt-3">
        <label>Tahun Penerbit</label>
        <input type="number" class="form-control" name="tahun_penerbit" value="{{$buku->tahun_penerbit}}" required>
    </div>
    <div class="form-group mt-3">
        <label>Stok Buku</label>
        <input type="number" class="form-control" name="stok" value="{{$buku->stok}}" required>
    </div>
    <div class="form-group mt-3">
        <label>Rak</label>
        <select class="form-control" name="rak_id">
            @foreach (\App\Models\Rak::get() as $rak)
                <option value="{{$rak->id}}" @if ($buku->rak_id == $rak->id) selected @endif>{{$rak->nama_rak}} - {{$rak->lokasi_rak}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary mt-3">Submit</button>
</form>

@endsection
