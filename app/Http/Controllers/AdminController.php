<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Pinjaman;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->role == 'anggota') {
            $masih_terpinjam = Pinjaman::where('tanggal_kembali', '=', null)->where('anggota_id', '=', auth()->user()->id)->get();
            $buku_popular = Buku::get();
            return view('dashboard', compact(['masih_terpinjam', 'buku_popular']));
        } elseif (auth()->user()->role == 'petugas') {
            $buku_popular = Buku::get();
            return view('dashboard', compact('buku_popular'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

}
