@extends('layouts.main')

@section('title', 'User')

@section('content')
<h1>Tambah User</h1>
<form action="{{ route('user.store') }}" method="POST">
    @csrf
    <div class="form-group mt-3">
        <label for="exampleInputName1">Nama</label>
        <input type="text" class="form-control" name="name" required>
    </div>
    <div class="form-group mt-3">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" name="email" required>
    </div>
    <div class="form-group mt-3">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" name="password" required>
    </div>
    <div class="form-group mt-3">
        <label for="exampleFormControlSelect1">Role</label>
        <select class="form-control" name="role">
          <option value="anggota">Anggota</option>
          <option value="petugas">Petugas</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary mt-3">Submit</button>
</form>

@endsection
