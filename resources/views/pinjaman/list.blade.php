@extends('layouts.main')

@section('title', 'Pinjaman')

@section('content')
<h1>Pinjaman</h1>
@if (auth()->user()->role == 'petugas')
    <a href="{{route('pinjaman.create')}}" class="btn btn-primary" role="button">Tambah pinjaman buku</a>
@endif
<table class="table table-striped border mt-3">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Peminjam</th>
            <th scope="col">Judul Buku</th>
            <th scope="col">Jumlah dipinjam</th>
            <th scope="col">Tanggal Pinjam</th>
            <th scope="col">Tanggal Rencana Kembali</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $key => $pinjam)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$pinjam->relasi_anggota->name}}</td>
            <td>{{$pinjam->relasi_buku->judul_buku}}</td>
            <td>{{$pinjam->jumlah}}</td>
            <td>{{date('d-M-Y', strtotime($pinjam->tanggal_pinjam))}}</td>
            <td>{{date('d-M-Y', strtotime($pinjam->tanggal_rencana_kembali))}}</td>
            <td>
                <form action="{{route('pinjaman.destroy', $pinjam->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <a href="{{route('pinjaman.show', $pinjam->id)}}" class="btn btn-info" role="button">Lihat</a>
                    @if (auth()->user()->role == 'petugas')
                        <a href="{{route('pinjaman.edit', $pinjam->id)}}" class="btn btn-warning" role="button">Edit</a>
                        <a href="{{route('pinjaman.destroy', $pinjam->id)}}" onclick="event.preventDefault(); this.closest('form').submit();" class="btn btn-danger" role="button">Hapus</a>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
