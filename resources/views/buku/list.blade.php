@extends('layouts.main')

@section('title', 'Buku')

@section('content')
<h1>Buku</h1>
@if (auth()->user()->role == 'petugas')
    <a href="{{route('buku.create')}}" class="btn btn-primary" role="button">Tambah buku</a>
@endif
<table class="table table-striped border mt-3">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Kode Buku</th>
            <th scope="col">Judul Buku</th>
            <th scope="col">Penulis Buku</th>
            <th scope="col">Penerbit Buku</th>
            <th scope="col">Tahun Terbit Buku</th>
            <th scope="col">Stok Buku</th>
            <th scope="col">Lokasi Buku</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $key => $buku)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$buku->kode_buku}}</td>
            <td>{{$buku->judul_buku}}</td>
            <td>{{$buku->penulis_buku}}</td>
            <td>{{$buku->penerbit_buku}}</td>
            <td>{{$buku->tahun_penerbit}}</td>
            <td>{{$buku->stok}}</td>
            <td>{{$buku->relasi_rak->nama_rak}} - {{$buku->relasi_rak->lokasi_rak}}</td>
            <td>
                <form action="{{route('buku.destroy', $buku->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <a href="{{route('buku.show', $buku->id)}}" class="btn btn-info" role="button">Lihat</a>
                    @if (auth()->user()->role == 'petugas')
                        <a href="{{route('buku.edit', $buku->id)}}" class="btn btn-warning" role="button">Edit</a>
                        <a href="{{route('buku.destroy', $buku->id)}}" onclick="event.preventDefault(); this.closest('form').submit();" class="btn btn-danger" role="button">Hapus</a>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
