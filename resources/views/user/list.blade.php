@extends('layouts.main')

@section('title', 'User')

@section('content')
<h1>User</h1>
@if (auth()->user()->role == 'petugas')
    <a href="{{route('user.create')}}" class="btn btn-primary" role="button">Tambah user</a>
@endif
<table class="table table-striped border mt-3">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $key => $user)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->role}}</td>
            <td>
                <form action="{{route('user.destroy', $user->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <a href="{{route('user.show', $user->id)}}" class="btn btn-info" role="button">Lihat</a>
                    <a href="{{route('user.edit', $user->id)}}" class="btn btn-warning" role="button">Edit</a>
                    @if (auth()->user()->role == 'petugas')
                        <a href="{{route('user.destroy', $user->id)}}" onclick="event.preventDefault(); this.closest('form').submit();" class="btn btn-danger" role="button">Hapus</a>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
