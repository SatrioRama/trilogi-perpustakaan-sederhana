@extends('layouts.main')

@section('title', 'Buku')

@section('content')
<h1>Tambah Buku</h1>
<form action="{{ route('buku.store') }}" method="POST">
    @csrf
    <div class="form-group mt-3">
        <label>Kode Buku</label>
        <input type="text" class="form-control" name="kode_buku" maxlength="5" required>
    </div>
    <div class="form-group mt-3">
        <label>Judul Buku</label>
        <input type="text" class="form-control" name="judul_buku" required>
    </div>
    <div class="form-group mt-3">
        <label>Penulis Buku</label>
        <input type="text" class="form-control" name="penulis_buku" required>
    </div>
    <div class="form-group mt-3">
        <label>Penerbit Buku</label>
        <input type="text" class="form-control" name="penerbit_buku" required>
    </div>
    <div class="form-group mt-3">
        <label>Tahun Penerbit</label>
        <input type="number" class="form-control" name="tahun_penerbit" required>
    </div>
    <div class="form-group mt-3">
        <label>Stok Buku</label>
        <input type="number" class="form-control" name="stok" required>
    </div>
    <div class="form-group mt-3">
        <label>Rak</label>
        <select class="form-control" name="rak_id">
            @foreach (\App\Models\Rak::get() as $rak)
                <option value="{{$rak->id}}">{{$rak->nama_rak}} - {{$rak->lokasi_rak}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary mt-3">Submit</button>
</form>

@endsection
